package com.pe.navegacionNavegacion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText eteTexto;
    Button butEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eteTexto = findViewById(R.id.eteTexto);
        butEnviar = findViewById(R.id.butEnviar);

        butEnviar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        //Accion para llamar otro Activity.
        Intent intent = new Intent(this, DetalleActivity.class);

        intent.putExtra("curso", eteTexto.getText().toString());
        intent.putExtra("edad", 27);


        startActivity(intent);

    }
}
