package com.pe.navegacionNavegacion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetalleActivity extends AppCompatActivity {

    TextView tviTexto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        tviTexto = findViewById(R.id.tviTexto);

        //Obtenemos intent del Activity Origen
        Intent intent = getIntent();

        //Obtenemos el valor de la llave desde el intent
        String texto = intent.getStringExtra("curso");
        int edad = intent.getIntExtra("edad",0);


        //Pintamos el texto obtenido en el TextView
        tviTexto.setText(texto + edad);
    }
}
